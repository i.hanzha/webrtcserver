const express = require("express");
const http = require("http");
const app = express();
const { Server } = require("socket.io");

const PORT = 8080;

const server = http.createServer(app);
const socketIO = new Server(server);

app.get("/client", function (req, res) {
  res.sendFile(__dirname + "/client.html");
});

var channels = {};
var clients = [];
var state = "Impossible";

socketIO.on("connection", function (socket) {
  const currentSessionId = socket.id;
  console.log("connected to server socket with socket.id ", socket.id);

  clients.push({ id: currentSessionId, socket: socket });
  if (clients.length >= 2) {
    state = "Ready";
  }
  notifyAboutStateUpdate();

  socket.on("STATE", function (msg) {
    console.log('STATE event');
    clients.find((c) => c.id === currentSessionId).emit("STATE", state);
  });

  socket.on("OFFER", function (msg) {
    console.log(`OFFER event [${currentSessionId}]`, msg);
    state = "Creating";
    notifyAboutStateUpdate();
    clients
      .filter((c) => c.id !== currentSessionId)
      .forEach((c) => c.socket.emit("OFFER", msg));
  });

  socket.on("ANSWER", function (msg) {
    console.log(`ANSWER event [${currentSessionId}]`, msg);
    clients
      .filter((c) => c.id !== currentSessionId)
      .forEach((c) => c.socket.emit("ANSWER", msg));
    state = "Active";
    notifyAboutStateUpdate();
  });

  socket.on("ICE", function (msg) {
    console.log(`ICE event [${currentSessionId}] `, msg);
    clients
      .filter((c) => c.id !== currentSessionId)
      .forEach((c) => c.socket.emit("ICE", msg));
  });
});

server.listen(PORT, null, function () {
  console.log("Listening on port " + PORT);
});

function notifyAboutStateUpdate() {
  clients.forEach(({ id, socket }) => {
    socket.emit("STATE", state);
  });
}
